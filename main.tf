provider "aws" {
  region = "us-east-1"
}

variable "subnet-cidr-block" {
    description = "subnet cidr block"
    default = "10.0.10.0/24"
    type = string
}

variable "vpc-cidr-block" {
  description = "vpc cidr block"
}

variable "environment" {
  description = "deployment environment"
}
resource "aws_vpc" "dev-vpc" {
  cidr_block = var.vpc-cidr-block
  tags = {
    Name = var.environment
    project = "terra-upskill"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.dev-vpc.id
  cidr_block = var.subnet-cidr-block
  availability_zone = "us-east-1a"
  tags = {
    Name = "subnet-1-dev"
  }
}

data "aws_vpc" "existing-vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing-vpc.id
  cidr_block = "172.31.96.0/20"
  availability_zone = "us-east-1a"
  tags = {
    Name = "default-subnet"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.dev-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}